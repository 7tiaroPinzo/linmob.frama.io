+++
title = "Weekly #LinuxPhone Update (17/2022): Plasma Mobile Gear 22.04, Nemo Mobile 0.9 and Phosh gesture support is inching closer!"
date = "2022-04-30T08:15:00Z"
[taxonomies]
tags = ["PinePhone","PinePhone Pro","Plasma Mobile","DanctNIX","Nemo Mobile",]
categories = ["weekly update"]
authors = ["peter"]
+++
It's saturday, [Linux App Summit](https://linuxappsummit.org/) is taking place. There's some bad news regarding open modem firmware and I am writing this in a train slowly but surely taking me to the sea for some time off.  
<!-- more -->

_Commentary in italics._

### Vacatitorial

While this weekly update is late, expect the next one two be even later - it may be as late as the evening of Monday, May 9th. Why? Well, I am taking some time off. No social media, no posts here. This also means that progress at LinuxPhoneApps.org will halt to a crawl - I may merge MRs, but that's it. A break is best taken full after all! 

### Software progress

#### GNOME ecosystem
* Gestures in Phosh are [getting closer](https://fosstodon.org/web/@agx@librem.one/108216840396338642)!
* This Week in GNOME: [#41 Italian Gestures](https://thisweek.gnome.org/posts/2022/04/twig-41/)
* chergert: [Builder GTK 4 Porting, Continued](https://blogs.gnome.org/chergert/2022/04/29/builder-gtk-4-porting-continued/).
* Ergaster: [Adopting Matrix at the GNOME Foundation](https://blog.ergaster.org/post/20220425-adopting-matrix/).
* Federico: [Paying technical debt in our accessibility infrastructure](https://viruta.org/paying-technical-debt.html). _You may think that this is boring, but it is really important. Free Software should be there for everybody!_

#### Plasma/Maui ecosystem
* Plasma Mobile: [Plasma Mobile Gear 22.04 is Out](https://plasma-mobile.org/2022/04/26/plasma-mobile-gear-22-04/). _Nice improvements! I've tried DanctNIX Plasma Mobile on my PinePhone this week, and boy is it smooth and stable these days, even on OG PinePhone!_
* Nate Graham: [This week in KDE: Porting everything to QtQuick](https://pointieststick.com/2022/04/29/this-week-in-kde-porting-everything-to-qtquick/).
* Nate Graham: [This week in KDE: Major accent color and Global Theme improvements](https://pointieststick.com/2022/04/22/this-week-in-kde-major-accent-color-and-global-theme-improvements/).
* MauiKit.org: [Maui Report 18](https://mauikit.org/blog/maui-report-18/). _Looking good!_

#### Nemo Mobile
* Jozef Mlích: [Nemomobile in April/2022](https://blog.mlich.cz/2022/04/nemomobile-in-april-2022/). _Another great progress report!_
* Nemo Mobile have published [new images](https://img.nemomobile.net/2022.05/). According to the [announcing tweet](https://twitter.com/neochapay/status/1520123621095714816), the next round of images is going to be 1.0!
* If you have a [CutiePi tablet, Nemo Mobile images](https://twitter.com/xmlich02/status/1517846605713580032) are available for you, too! _[TuxPhones have tried it](https://twitter.com/tuxphones/status/1518646970914684928), looking good!_

#### Distro news
* [DanctNIX is moving towards Tow-Boot on the PinePhone Pro](https://fosstodon.org/@danctnix/108212376597040856).

#### Firmware
* hughsie: [fwupd 1.8.0 and 50 million updates](https://blogs.gnome.org/hughsie/2022/04/28/fwupd-1-8-0-and-50-million-updates/).
* Speeking of Firmware, Dylan van Assche alerted me about the [fact that Quectel is working on Secure Boot for their modems](https://github.com/fwupd/fwupd/commit/17854099d0e614c06b5a40d2477477ee3d850fc7#diff-5a375f230ee85cf307402aaabd8da6e6dbc8ad32e0a5e9f6d302a896a8387c4cR557) to lock them down, likely rendering the installation of Biktorgj's firmware impossible going forward.
#### Mesa stuff
* Phoronix: [Panfrost Lands Valhall Driver Code For Mesa 22.2](https://www.phoronix.com/scan.php?page=news_item&px=Panfrost-More-Valhall-Mesa-22.2). 
* Phoronix: [Mesa Can Now Be Built With Select Video Codecs Disabled For Software Patent Concerns](https://www.phoronix.com/scan.php?page=news_item&px=Mesa-Optional-Video-Codecs).

### Worth noting

### Worth reading
#### App reviews
* Jason123Santa: [Review of Delta Chat messenger](https://jasonsanta.xyz/posts/review-of-delta-chat-messenger-.html). _Nice post! See also: [1](https://linuxphoneapps.org/apps/chat.delta.desktop/), [2](https://linuxphoneapps.org/apps/chat.delta.kdeltachat/)._

* Purism: [Improving the Stability and Reliability with a Modular Modem in the Librem 5](https://puri.sm/posts/stability-and-reliability-with-a-modular-modem-in-the-librem-5/)

### Worth watching
#### Phosh and Apps 
* Bitter Epic Productions: [The Apps of Phosh on my PinePhone Pro. Do they work?](https://www.youtube.com/watch?v=dPmJh8s8GuY). _Nice walkthrough!_

#### PinePhone Pro Gaming
* Tiziano Bacocco: [Amnesia the Dark descent running on pinephone pro](https://www.youtube.com/watch?v=I615NkHFdV8).

#### PinePhone running Wine
* Ivon Huang: [PinePhone執行Windows程式exe (Box86 + Wine)](https://www.youtube.com/watch?v=KmWUxyP9ufY).


#### Ubuntu Touch
* Leszek Lesner: [Volla Phone with Ubuntu Touch after 2 years](https://www.youtube.com/watch?v=Enc_oMadg1U).

#### Sailfish OS 
* Tech with Jowe: [Sailfish OS Review | Is This The Linux Phone For You?](https://www.youtube.com/watch?v=FROPcbINd1M).

#### Just apps
* baby WOGUE: [headerbar-LESS apps: the next big trend in GNOME design!!](https://www.youtube.com/watch?v=wHU811igDcU)

#### Drawing
* The Trash Can 👍: [Timelapse Art with GNOME Draw on Pinephone - Some Kinda Farm](https://diode.zone/w/61adf6ea-6c5d-4a16-8895-aa4fa3d04109). _Nice :)_

#### Linux App Summit
* Julian Sparber: [Fractal-next: The journey of rewriting a GTK Matrix client](https://youtu.be/CBPefa0Ckq8?t=9071). _There's more in this Day 1 stream that may be of interest, check the [timetable](https://conf.linuxappsummit.org/event/4/timetable/#all). The day 2 stream is [here](https://www.youtube.com/watch?v=HxM15UOVmyA)._

#### Shorts
* Avenitson: [Pinephone unboxing](https://www.youtube.com/shorts/InjH3Mc0Y3U)

### Something missing?
If your projects' cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email!

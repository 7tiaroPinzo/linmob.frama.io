+++
title = "Weekly GNU-like Mobile Linux Update (35/2022): Phosh 0.21.0, postmarketOS 22.06.2 and progress reports on Kaidan and Nemo Mobile"
date = "2022-09-05T18:45:00Z"
draft = false
[taxonomies]
tags = ["PinePhone","PinePhone Pro","Kaidan","Matrix","MNT Pocket Reform","GNOME Shell Mobile","Phosh","postmarketOS",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " and hedgedoc contributors"
+++
News on GNOME Shell Mobile, a new UI for GNOME Calls and a lot more.
<!-- more -->
_Commentary in italics._

Let's start this with a __giant thank you__ to everyone who emailed me and contributed to this update on the pad linked in the previous blog post! Thank you all for reaching out - it really helps with staying motivated and seeing positive things in life!

### Hardware

* [E Ink prototype for Matrix chat](https://www.hackster.io/news/sqfmi-s-franky-gets-a-blackberry-keyboard-upgrade-becomes-a-pocket-friendly-beeper-chat-gadget-372b812d6fb0). _As a Beeper (the Matrix based messaging service) user, I really like to see some hardware :) (Also: Really glad that I can't buy this (yet), eInk + Matrix would totally entice me to buy something I really don't need.)_
* [MNT Pocket Reform](https://mntre.com/media/reform_md/2022-06-20-introducing-mnt-pocket-reform.html) - Will ship soon, includes 4G/5G/LTE modem!

### Software progress

#### GNOME ecosystem

* This Week in GNOME: [#59 Beautiful Calls](https://thisweek.gnome.org/posts/2022/09/twig-59/)
* Phoronix: [GNOME Shell & Mutter 43 Release Candidates Bring Last Minute Changes - Phoronix](https://www.phoronix.com/news/GNOME-Shell-Mutter-43-RC)

##### Releases and pre-releases
* [devrtz: "Calls 43.rc.0 was released 📲 🎉 Mainly bug fixe…" - Fosstodon](https://fosstodon.org/@devrtz/108938987511483619)
* [Guido Günther: "phosh 0.21.0 is out 🚀📱 : It was supposed to be…" - Librem Social](https://social.librem.one/@agx/108922949028367765)
* [Guido Günther: "Along with that go releases of phosh-mobile-setti…" - Librem Social](https://social.librem.one/@agx/108922957285902931)
* [Sebastian Krzyszkowiak: "phoc 0.21.1 has been released, fixing things that…" - Librem Social](https://social.librem.one/@dos/108922460508207139)

#### Plasma/Maui ecosystem
* [This week in KDE: day color – Adventures in Linux and KDE](https://pointieststick.com/2022/09/02/this-week-in-kde-day-color/)
* [Kaidan's End-to-End Encryption Trust Management - Kaidan](https://www.kaidan.im/2022/08/31/e2ee-trust-management/)
* [Encrypted Audio and Video Calls - Kaidan](https://www.kaidan.im/2022/09/03/audio-video-calls/)
* [Maui 2.2.0 Release – MauiKit — #UIFramework](https://mauikit.org/blog/maui-2-2-0-release/). _Beautiful!_

#### Ubuntu Touch
* UBports blog: [UBports Training Update 6 aka 0110: A new season is on its way!](https://ubports.com/de/blog/ubports-blogs-nachrichten-1/post/ubports-training-update-6-aka-0110-a-new-season-is-on-its-way-3862)

#### Nemo Mobile
* [Nemomobile in August 2022](https://nemomobile.net/pages/nemomobile-in-august-2022/). _I really need to try this again!_

#### Sxmo
* [Sxmo 1.11.0 released — sourcehut lists](https://lists.sr.ht/~mil/sxmo-announce/%3C87k06k2r0b.fsf%40momi.ca%3E). _Nice improvements!_

#### Distributions
* postmarketOS blog: [v22.06 SP2: The One That Swipes](https://postmarketos.org/blog/2022/09/04/v22.06.2-release/). _A very welcome update. My personal highlight on Phosh is that postmarketOS Welcome comes up after updating and rebooting to educate users about swiping. Really well done!_

#### Firmware updating
* [New fwupd 1.8.4 release – Technical Blog of Richard Hughes](https://blogs.gnome.org/hughsie/2022/08/30/new-fwupd-1-8-4-release/)
* [Speeding up getting firmware updates to end users – Technical Blog of Richard Hughes](https://blogs.gnome.org/hughsie/2022/09/02/speeding-up-getting-firmware-updates-to-end-users/)

#### Linux kernel
* [There's some PinePhone Pro Device Tree that has been accepted into mainline Linux](https://lore.kernel.org/all/166231195330.2423948.5394487959576836993.b4-ty@sntech.de/). _Great work and congrats, everybody!_

#### Non-Linux
* Lup Yuen Lee: [NuttX RTOS on PinePhone: Fixing the Interrupts](https://lupyuen.github.io/articles/interrupt)
* Genodians: [Genode - Release notes for the Genode OS Framework 22.08](https://genode.org/documentation/release-notes/22.08#Genode_coming_to_the_phone)

#### SDL and PipeWire
* [sdl12-compat 1.2.54 Pre-Release Gets More Games Running On This SDL2 Compatibility Layer - Phoronix](https://www.phoronix.com/news/sdl12-compat-1.254)
* [PipeWire 0.3.57 Adds AAC Decoder, Opus For Bluetooth - Phoronix](https://www.phoronix.com/news/PipeWire-0.3.57-Released)

#### Matrix
* [This Week In Matrix 2022-09-02](https://matrix.org/blog/2022/09/02/this-week-in-matrix-2022-09-02)


### Worth noting
* Some postmarketOS edge breakage (partially resolved, check linked issues):
  * [glGetString segmentation faults on ARMv7](https://postmarketos.org/edge/2022/08/31/glgetstring-armv7-segfault/)
  * [Samsung Galaxy S III (samsung-m0): Phosh crashes](https://postmarketos.org/edge/2022/08/31/samsung-m0-phosh/)
  * [Upower 1.90.0 broke battery reporting on many devices](https://postmarketos.org/edge/2022/09/04/upower-update-battery-reporting/)
* [Waydroid on the Librem 5 is making progress](https://social.librem.one/@dos/108918573743188321), [more details](https://teddit.net/r/Purism/comments/x2k3yj/comment/imn4mvb/?context=3).
* [If you want to theme libadwaita ...](https://social.linux.pizza/@apathetic_bystander/108945498885252153)


### Worth reading

#### Bicycle computing
* dcz: [Jazda: Rust on my bike](https://dcz_self.gitlab.io/posts/jazda_rust/). _Great project!_


#### Praise for some hardware
* TuxPhones: [Linux machines that just work](https://tuxphones.com/linux-mobile-devices-just-work/)


#### One for the XMPP enthusiats
* nicoco.fr: [The first beta of Slidge (XMPP bridges) is out!](https://nicoco.fr/blog/2022/09/04/slidge-first-beta/)

#### Maybe worth reading
* LINMOB: [Re-Evaluating Priorities](https://linmob.net/re-evaluating-priorities/). _Just so that you aren't too surprised why I'll tweet and toot a lot less than I used to._


### Worth listening

### Worth watching

#### GNOME Shell Mobile

* Tobias Bernard: [GNOME Shell Mobile preview](https://mastodon.social/@tbernard/108912261224740915). _Impressive video, really smooth - this is being demoed on a PinePhone Pro. The work was funded by the German Prototype Fund, and [presented last week](https://twitter.com/nikodunk/status/1564318177206476800) - funding is now, sadly, over. If you want to try the changes, these should be the places to go to: [Mutter](https://gitlab.gnome.org/verdre/mutter/-/tree/mobile-shell), [GNOME Shell](https://gitlab.gnome.org/verdre/gnome-shell/-/tree/mobile-shell). Now, I get it, you want to run this, right? Looks like it might be [easier to test somewhat soon](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/merge_requests/1727#note_1545696)!_

#### PinePhone Pro: Taking photos with Megapixels!
* Wolf Fur Programming: [Pinephone Pro Camera working with megapixel](https://youtu.be/y8zKux7vxCg). _Great, camera support without a proprietary app! (Also, it's Megapixel__s__, but hey, typos happen!)_

#### Other PinePhone Pro improvements
* Wolf Fur Programming: [Pinephone Pro Wake from Suspend Audio Working And Flutter works!](https://youtu.be/3gqship6Xko). _Great news, working audio after suspend was a big one, and Mobian is a good distro. That Flutter thing - I noticed it worked again, but did not point it out enough, I guess!_


#### Virtual keyboards
* Bitter Epic Productions: [Linux Phone Keyboard Wars! Does Plasma Mobile or Phosh Have the better keyboard?](https://youtu.be/xtl9hPzPDfA). _Fun as always! Squeekboard does have some Emoji on my phone though ;-)_

#### Non-Linux OS on PinePhone
* Lup Yuen Lee: [BASIC blinks the #PinePhone LEDs ... On Apache #NuttX RTOS 🤩](https://www.youtube.com/watch?v=OTIHMIRd1s4)

#### Not just on mobile, but...
* Geotechland: [GNOME Theming Is Finally Here](https://www.youtube.com/watch?v=uOcdxBXGVXA)

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email - or just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A) for the next one!

PS: In case you are wondering about the title [...](https://fosstodon.org/@linmob/108516506484897358)

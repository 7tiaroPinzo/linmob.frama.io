+++
author = "peter"
title = "Random Thoughts on the future of LINMOB"
aliases = ["2012/06/26/random-thoughts-future"]
comments = true
date = "2012-06-26T21:23:00Z"
layout = "post"
[taxonomies]
tags = ["ASUS", "future", "Google Nexus", "high resolution screens", "medfield", "mirasol", "Nook Simple Touch", "orange san diego", "pixelqi", "quadcore smartphones", "retina screens", "sony ericsson xperia pro", "Tegra3","Nokia N900",]
categories = ["internal","personal"]
authors = ["peter"]
+++
It is one day before the announcement of a new Nexus Tablet, supposedly 7&#8221;, Tegra 3 and all&mdash;it's all over the internet, if you care. I didn't cover the last big trade shows at all, and over time I have had many devices in my hands which I didn't cover except maybe for mentioning them in a few tweets or lines (Nokia N900, LG Optimus Speed, Sony Ericsson XPERIA Pro, Lenovo A750, Jiayu G2).

Honestly, I even shot a few videos I never published, because they were simply not good enough to be published unedited&mdash;and I didn't prefer shooting it again last year for no reason&mdash;I am not that experienced at video handling, as I had to realize recently I can't even rip DVDs without messing it up&mdash;audio is never in sync. I hate my own laziness, however, there are simply more important things in my life that need to be done than writing down (or saying on video) what exactly I think about a certain phone screen, or why this app is better than the other one. It's not that work or private life steal the time of this blog, it's simply that I am bored of most of what's happening.

Don't get me wrong: The mobile space has rarely been more dynamic in the past, not only talking of what one could call &#8220;spec sheet madness&#8221; (imagine someone would have told you in 2002 that there would be quad core phones with 2 Gigabytes of RAM in 2012). With Android dominant on smartphones, but not tablets and the soon to happen launch of Windows 8 and Windows Phone 8 with more interesting convertible devices which really come close to what I dreamt of 5 years ago (and which than seemed really unlikely to happen, imagine how poorly (in terms of performance) an ASUS PadFone would have been back then)), with Intel still trying to find a place in your trousers pocket (initial <a href="http://www.theverge.com/2012/6/14/3078078/orange-san-diego-intel-medfield-review">reviews of 1st gen Medfield phones like the Orange San Diego</a> haven't been overly positive, though mostly because the overall package is not great) and MIPS is trying to get a foot in the mobile markets, too.

Then there is digital reading, a topic that interests me really and has made my buy not only Apple's iPad 2 in last september, but recently a Barnes&amp;Noble Nook Simple Touch, which&mdash;as you must know to understand that I as a german guy bought this US-Reader, runs Android 2.1 (Eclair) and thus is hackable to a certain extend. Display technology is not really going much forward right now&mdash;while AMOLED and LCDs have been getting better and better, delivering better pictures at higher resolutions (300ppi may be average in mid range smartphones in 2013), power saving reflective technologies optimized for sunlight reading like PixelQi and Qualcomms Mirasol technology have not been exactly successful yet.

Still, finding the time to even write stupid stuff like this post will continue to happen only on rare occasions. So don't expect anything of this blog in the near foreseeable future. I may post something, but more likely, I will not._

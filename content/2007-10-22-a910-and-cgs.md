+++
title = "A910 and CGs"
aliases = ["2007/10/a910-and-cgs.html"]
author = "peter"
date = "2007-10-22T16:14:00Z"
layout = "post"
[taxonomies]
tags = ["EZX/LJ", "ezx_wlan.cfg", "modding", "Motorola A910",]
categories = ["projects"]
authors = ["peter"]
+++
Today I found the time to get back onto A910 modding. I managed to get to know some things, and after writing this I'll go on with research.
<!-- more -->

_First:_ Code Group (CG) names differ from A910 to other devices, they are the same as on some A1200 (newer firmwares, if I got that right). For finding out things I had a look at `ilove3d-48p-ultimate` for the Motorola Rokr E2, and compared the stuff I found there using the SBF-extraction tool you find over at freemod.net, results (compared to `A910-R57_G_10.08.07R`):

CG34/CG37 contents seem to be nearly the same, but as there is no CG41, CG42 is what CG41 is on E2 and so on. That means, that quite important CG43 folder from backup (made with some backup tool for E2 from e2mod.com) (contains main file system&#8230;), is CG44 in firmware.

Unfortunately I wasn't successfull yet on extracting files from E2-CG43/A910-CG44&mdash;I'll google on that, and I'm pretty sure that someday I might find out how to get this work.

Maybe you remember this:

<blockquote>CG42 contains `/usr/setup/`, and there is a file called `ezx_wlan.cfg`. Line 4 of this file contains the following text:
~~~
WiFiUIConnectUMAN = 1
~~~
</blockquote><br />

After my research today I know why changing this value to zero and flashing it only caused languages to disappear: I simply flashed it to the wrong position. Shame on me. Now I will have to try this out again.

+++
title = "Necunos NC_1 or a Lunchbox Smartphone?"
aliases = ["2019/01/05/necunos-nc_1-or-a-lunchbox-smartphone.html"]
author = "Peter"
date = "2019-01-05T00:10:06Z"
layout = "post"
[taxonomies]
tags = ["building stuff", "DIY", "devices", "i.MX6", "Librem 5", "linux", "making", "necuno", "Necunos NC_1", "PDA", "Raspberry Pi", "smartphones"]
categories = ["hardware"]
authors = ["peter"]
+++
_While Purism has allegedly finally managed to ship out their developer kits, Necunos will provide you with a thingy they call a smartphone much faster._<!-- more -->[^1] 

__Necunos NC_1__

<del><em>The Necunos project may be a scam. Be careful!</em></del>
<em> Update 2/3/2019: After FOSDEM it seems a little more real. Necuno Solutions plan to ship the NC_1 in March. Let's see.</em>

__Specifications:__

* SoC: NXP i.MX Quad, ARM Cortex A9 quadcore @ 1.2 GHz, Vivante GPU (Etnaviv driver, hardware acceleration)
* 1 GB Ram _(meh!)_
* 8 GB Storage
* 3500 mAh Battery
* 5,0" display _(no resolution given)_
* Aluminum body
* 5 MP Camera
* Audio: 3.5mm audio jack
* Charging: Micro-USB, Data transfer disabled
* Microphone: Built-in microphone
* Speakers: 2 Built-in speaker
* WLAN: WiFi (via SDIO) WL1801 (2.4 GHz)
* Ethernet: High speed 100Mb/s
* Serial: Internal
* Closed source firmware _with memory access_: NO
* Binary blobs: NO
* Locked bootloader: NO
* Operating Systems: Multiple community driven operating systems to choose from.
  * <a href="https://www.plasma-mobile.org">Plasma Mobile</a> on Debian
  * Plasma Mobile on <a href="https://postmarketos.org/">postmarketOS</a>
  * <a href="https://leste.maemo.org/Main_Page">Maemo Leste</a>
  * <a href="https://wiki.merproject.org/wiki/Nemo">Nemo Mobile</a>
  * <a href="https://en.wikipedia.org/wiki/LuneOS">Lune OS</a>
  * maybe <a href="https://www.replicant.us/">Replicant later?</a>

__Price:__ 1199 EUR

Um. Yeah. That is pricey, and you don't get a phone, but a WiFi only thing for open source OS. While their argument, that mobile connectivity is a privacy and security concern is true, a "phone" without any mobile connectivity is a problem. Also, it is saddening to see a WLan chip used that requires the use of proprietary firmware, where "free as in free software" alternatives exist.[^2] With this device, you practically need to carry another device if you any kind of communications service in areas without open WiFi coverage which – at least here in Germany – feels like practically everywhere.
_I really don't know how this is much better than what I will continue with, besides the fact that the __NC_1__ will likely feel like an actual product instead of something you cobbled together._


__"Butterbrotdosen-Smartphone"__ is a talk by Bücherratte that took place at the 35th Chaos Communication Congress (which I attended) &mdash; <a href="https://media.ccc.de/v/35c3-9681-butterbrotdosen-smartphone">go watch the recording</a>. It details the process of building a "smartphone" using a Raspberry Pi 3, a 4" Waveshare GPIO-connected HVGA (320 x 480 pixels) resistive touchscreen, an USB soundcard plus a power bank for power supply. All that stuff is arranged into a tiny lunchbox. It runs Arch Linux ARM, but could essentially run anything that supports the Pi and the display. Phone calls are done with LinPhone, a popular SIP softfone and require the headset to be plugged in.

Sure, this seems terrible, and the fact, that Arch Linux ARM is used instead one of all these FOSS phone distributions is certainly terrible. But the BOM is a lot cheaper, it should be buildable with a budget of about 100 Euros.

__Remarks/ideas on "building your own smartphone/gadget/pda/portable terminal"__

If you are considering such a build, I would recommend to tweak a few things: Use a soundcard that has a combined plug for headset and microphone – if you can't find any, resort to the <a href="https://github.com/boboianbobo/piusbc/wiki/Apple-USB-C-to-3.5-mm-on-the-Raspberry-Pi">USB-C iPad Dongle</a>. This will enable you to use less bulky headsets. Consider using a battery designed for use with the Pi. Use an HDMI connected display, GUI will be a lot better, you'll be able to play games or watch video. There are 3.5" IPS WVGA (800x480 pixels) displays available. That is not an amazing screen resolution, but it should make your life a whole lot easier when dealing with "desktop apps". Get a really fast microSDXC card, that is known to work fast with the Pi and can survice being used for swap purposes. Employ technologies like <a href="https://en.wikipedia.org/wiki/Zram">zram</a>.

Consider using a different hardware design, especially if you can get your hands on a 3D-printer, read the following links for inspiraton:

* <a href="https://n-o-d-e.net/terminal_3.html">N-O-D-E: The Handheld Linux Terminal Version 3</a>
* <a href="https://n-o-d-e.net/zeroterminal.html">N-O-D-E: The Zero-Terminal (Portable Pi Zero Terminal)</a>
* <a href="https://www.kickstarter.com/projects/hash42/noodle-pi-the-complete-raspberry-pi-pocket-compute">Kickstarter: NoodlePi</a>
* <a href="https://www.instructables.com/id/Pocket-Sized-Linux-Computer-Pi-Micro/">instructables: Pi Micro</a>

Now, why do I recommed to stick to a Raspberry Pi?[^3]  While there are a ton of other SBCs out there, few have as good support, especially with regards to accelerated graphics and the ecosystem of peripherals and tutorials. NXP i.MX 6 based SBCs would be a great option, but they are prohibitively expensive. Tiny boards – e.g. the Raspberry Pi Zero W, Nano Pi or tiny Orange Pi Boards, that would be great for projects like these – often top out at 512 MB Ram, making them difficult to recommend, when standard distributions with standard desktops apps are what you are aiming at. It really depends on the software you want to use, though. Just consider all these routers with 32 MB Ram. _The sky is the limit!_


[^1]: To me, this &mdash; while it is an admirable effort &mdash; is not a smartphone, but rather a modern Linux PDA since it has no modem to connect to cell services. I'll keep sticking to this 22 year old definition, where a smartphone has to be an actual phone.

[^2]: ath9k-htc, anyone? Only caveat: USB instead of SDIO.

[^3]: BTW, if you have one, I would recommend an old Pi 2 B, and use some USB Wifi-Dongle for openness and power consumption/heat.

+++
title = "Having a stand at Linux-Infotag 2023"
date = "2023-05-03T18:45:00Z"
draft = false
[taxonomies]
tags = ["Linux-Infotag", "stand", "Sailfish OS", "Librem 5", "postmarketOS", "Mobian", "Ubuntu Touch"]
categories = ["events", "personal",]
authors = ["Peter"]
[extra]
edit_post_with_images = true
+++

As [announced](https://linmob.net/see-you-in-augsburg-lit-2023/), Ollie, cahfofpai and I set up a "Linux on Smartphones"-stand. It was a blast to talk about our collective hobby.[^1] 

<!-- more -->

<mark>If you want first hand impressions, and can understand German, give [this podcast episode](https://techniktechnik.de/?podcast=tt173-augsburger-linux-infotag-2023) a listen!</mark>

Thanks to cahfofpai and Ollie, we were well prepared and [had a nice flyer](https://pinephone.de) and labels to explain some device features &mdash; thanks for doing this with me, guys!


This is what our little table looked like:

{{ gallery() }}

### How it went

While this was for fun and we did not define objectives beforehand, if we had defined objectives, they likely would have been: 

- Raising awareness about "Mobile Linux",
- answering questions/demoing things for those aware/on the fence,
- talking to/helping out people who are already using Mobile Linux.

Looking at what I recall being asked, there were a few common themes:

- "I want to see the mobile Ubuntu"/Is this still a thing?,
- "I want to see the PinePhone!",
- a few people had fond memories of the Nokia N900,
- "What, no camera?[^2] That's sad!",
- and one or two persons remembered Openmoko,
- people looking at the LINMOB stickers and uttering "I can't read that, what does it mean".

These were rather unsurprising, so let's have a few points I found interesting or fun:

- Sxmo being perceived really positively due to 'less icons, and thus potentially less adictive',[^3]
- one guy really liked to try different UI paradigms and kept asking for different mobile shells to try,
- sustainability was much more a topic than I would have imagined: Longer device lifecycles while maintaining device security was a big topic, even for those looking to stay on Android.

And, finally, a very important one:

- "I don't want to/know how to flash my device."/"Can I buy a device with this pre-installed?"

&mdash; as a person that modified device software before Android and fastboot became a thing, this is something I tend to forget.

### (Potential) room for improvement

We had a great time, but I think this is what could be improved, even if that _is_ not going to be easy:

- point to organizations that provide devices pre-installed or help with installing and setting up mobile Linux,[^4] ideally locally, or alternatively, help people with doing this at the stand or (better, to reduce interuptions) in a separate workshop session (similar to the FSFE's Upcycling Android'),
- be better prepared (if possible) for questions around 'which device to get?', 'what can I do with my old phone here',
- have better connectivity (I thought I was prepared for this, but both my mobile carriers only offered 2G connectivity at the venue).

Of course, help and feedback regarding further improvement is duely appreciated. If you are aware of projects/workshops/events that help people get into Linux on Mobile or want to help with doing so, please get in touch via email!

### Personal thoughts

It was really great to talk to so many people. Of course, nerding out and talking about experiences (I talked to people familiar with Sailfish OS, Ubuntu Touch and Sxmo and helped someone running postmarketOS Phosh with connecting a Nextcloud via GNOME Online Accounts) was just the best and worth it alone. 

I definitely hope to repeat this at another Linux event &mdash; preferably one that does not last too long. It is a bit exhausting after all (I slept most of the next day). Doing this a second day would have been too much, just look at this picture:

{{ figure(img_name="2023-05-03-lit-2023-stand-experiences/04-late-afternoon.jpeg", alt="A tired Peter", width=700) }}

One could solve this by having enough people taking shifts. Overall, I really look forward to doing this again at future events!

[^1]: We were asked about what kind of group or organization we are a number of times, and apparently it's perceived as weird to just do such a thing without a formal organization.

[^2]: Refering to mainlined Qualcomm devices.

[^3]: The idea was to then use [Waydroid](https://waydro.id/) for a handful of non-avoidable' apps.

[^4]: The FSFE's [Upcycling Android campaign](https://fsfe.org/activities/upcyclingandroid/upcyclingandroid.en.html), [Topio e.V.](https://www.topio.info/), [PINE64eu](https://pine64eu.com),  [Volla](https://volla.online), [Jolla-Devices.com](https://buy.jolla-devices.com/) come to mind. 

+++
title = "A910: Success!"
aliases = ["2008/04/a910-success.html"]
date = "2008-04-16T12:41:00Z"
[taxonomies]
tags = ["A910", "A910i", "i-geekmod", "modding", "vpnc"]
categories = ["projects"]
authors = ["peter"]
+++

WiFi can be used to surf the internet now, as I finally managed to build a A910i based firmware. I'll release it, but as I plan to integrate some useful E2-Apps, I won't do that now&mdash;r1 is send to some beta testers ;)
<!-- more -->

I'm really happy that this works now, as I was looking forward to have wifi for internet access on A910 for some months now. It works, just works&#8230; :-)

Now I think about porting vpnc to A910, I already read some cross compiling howtos&mdash;it'd be really nice to get vpnc on A910 as my universities' wireless network is Cisco protected.

Here is what I found so far:

* <a href="http://www.liebchen-online.de/vpn-zaurus-cisco_en.html">vpnc on &#8216;Zaurus&#8217;</a>
* <a href="http://www.unix-ag.uni-kl.de/~massar/vpnc/">vpnc-homepage</a>

The greatest problem (I assume) will be the `tun`-device.

As always: If you've got any ideas, please contact me!
